const _ = require("lodash");
const path = require("path-parser");
const { URL } = require("url");
const mongoose = require("mongoose");
const authMiddleware = require("../middlewares/auth");
const creditsMiddleware = require("../middlewares/requireCredits");
const Mailer = require("../services/Mailer");
const surveyTempalte = require("../services/emailTemplates/emailTemplate");
const { Path } = require("path-parser");

const Survey = mongoose.model("surveys");

module.exports = (app) => {
  app.get("/api/surveys", authMiddleware, async (req, res) => {
    const surveys = await Survey.find({ _user: req.user.id }).select({
      recipients: false,
    });

    res.send(surveys);
  });

  app.get("/api/surveys/:surveyId/:choice", (req, res) => {
    res.send("Thanks for voting");
  });

  app.post("/api/surveys/webhooks", (req, res) => {
    const events = _.map(req.body, (event) => {
      const pathName = new URL(event.url).pathname;
      const parser = new Path("/api/surveys/:surveyId/:choice");
      const match = parser.test(pathName);
      if (match) {
        return { email: event.email, ...match };
      }
    });

    const compactEvents = _.compact(events);
    const uniqueEvents = _.uniqBy(compactEvents, "email", "surveyId");

    uniqueEvents.forEach(async (event) => {
      await Survey.updateOne(
        {
          _id: event.surveyId,
          recipients: {
            $elemMatch: { email: event.email, respondend: false },
          },
        },
        {
          $inc: { [event.choice]: 1 },
          $set: { "recipients.$.respondend": true },
        }
      ).exec();
    });

    res.send({});
  });

  app.post(
    "/api/surveys",
    authMiddleware,
    creditsMiddleware,
    async (req, res) => {
      const { title, subject, body, recipients } = req.body;

      const survey = new Survey({
        title,
        body,
        subject,
        recipients: recipients
          .split(",")
          .map((email) => ({ email: email.trim() })),
        _user: req.user.id,
        dateSent: Date.now(),
      });

      const mailer = new Mailer(survey, surveyTempalte(survey));

      try {
        await mailer.send();
        await survey.save();
        req.user.credits -= 1;

        const user = await req.user.save();
        res.send(user);
      } catch (error) {
        res.status(422).send(error);
      }
    }
  );
};
