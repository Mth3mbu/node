const configs = require("./../config/keys");
const stripe = require("stripe")(configs.stripeSecreteKey);
const authMiddleware = require("../middlewares/auth");

module.exports = (app) => {
  app.post("/api/stripe", authMiddleware, async (req, res) => {
    const charge = await stripe.charges.create({
      amount: 500,
      currency: "usd",
      description: "$5 for 5 credits",
      source: req.body.id,
    });

    req.user.credits += 5;
    const user = await req.user.save();

    res.send(user);
  });
};
