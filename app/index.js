const express = require("express");
const cookieSession = require("cookie-session");
const passport = require("passport");
const mongoose = require("mongoose");
const configs = require("./config/keys");
const bodyParser = require("body-parser");
require("./models/User");
require("./models/Survey");
require("./services/passport");

const authRoutes = require("./routes/authRoutes");
const billingRoutes = require("./routes/billingRoutes");

mongoose.connect(configs.mongoURI);

const app = express();

app.use(bodyParser.json());

app.use(
  cookieSession({
    maxAge: 30 * 20 * 60 * 60 * 1000,
    keys: [configs.cookieKey],
  })
);

app.use(passport.initialize());
app.use(passport.session());

authRoutes(app);
billingRoutes(app);
require("./routes/surveyRoutes")(app);

app.listen(process.env.PORT || 5000);
