import "./App.css";
import React, { Component } from "react";
import Header from "./components/Header";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "./actions";
import Landing from "./components/Landing";
import DashBoard from "./components/Dashboard";
import SurveyNew from "./components/surveys/NewSurvey";
class App extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <Header />
          <div className="container">
            <Route path="/" exact={true} component={Landing}></Route>
            <Route path="/surveys" exact component={DashBoard}></Route>
            <Route path="/surveys/new" component={SurveyNew}></Route>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, actions)(App);
