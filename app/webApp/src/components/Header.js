import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Billing from "./Billing";

class Header extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return "Loading....";
      case false:
        return <a href="/auth/google">Login With Google</a>;
      default:
        return [
          <li key="001">
            <Billing />
          </li>,
          <li key="002" style={{ margin: "0 10px" }}>
            credits: {this.props.auth.credits}
          </li>,
          <li key="003">
            <a href="/api/logout">Logout</a>
          </li>,
        ];
    }
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper">
          <Link
            to={this.props.auth ? "/surveys" : "/"}
            className="brand-logo"
          >
            Emaily
          </Link>
          <ul id="nav-mobile" className="right hide-on-med-and-down">
            {this.renderContent()}
          </ul>
        </div>
      </nav>
    );
  }
}

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(Header);
