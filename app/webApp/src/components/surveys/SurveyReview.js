import _ from "lodash";
import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import formFields from "./formFields";
import * as actions from "../../actions";

const SurveyReview = (props) => {
  const reviewContent = _.map(formFields, (field) => {
    return (
      <div key={field.name} style={{ margin: 10 }}>
        <label>{field.label}</label>
        <div>{props.formValues[field.name]}</div>
      </div>
    );
  });

  return (
    <div className="container">
      <div className="row">
        <h5>Please confirm your entries</h5>
        {reviewContent}
        <button
          className="yellow darken-3 btn-flat left white-text"
          onClick={props.onBack}
        >
          Back
        </button>
        <button
          type="submit"
          className="green btn-flat right white-text"
          onClick={() => props.submitSurvey(props.formValues, props.history)}
        >
          Send Survey <i className="material-icons right">email</i>
        </button>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return { formValues: state.form.surveyForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(SurveyReview));
