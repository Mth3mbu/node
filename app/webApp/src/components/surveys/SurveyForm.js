import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import SurveyField from "./SurveyField";
import { Link } from "react-router-dom";
import emalisValidation from "../../utils/emailsValidation";
import formFields from "./formFields";

const FIELDS = [
  { label: "Survey Title", name: "title" },
  { label: "Subject", name: "subject" },
  { label: "Email Body", name: "body" },
  { label: "Recipient List", name: "emails" },
];

class SurveyForm extends Component {
  renderFields() {
    return formFields.map(({ label, name }) => {
      return (
        <Field
          key={name}
          type="text"
          name={name}
          component={SurveyField}
          label={label}
        />
      );
    });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <form onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}>
            {this.renderFields()}
            <Link
              to="/surveys"
              type="submit"
              className="red btn-flat left white-text"
            >
              Cancel
            </Link>
            <button type="submit" className="teal btn-flat right white-text">
              Next <i className="material-icons right">done</i>
            </button>
          </form>
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  formFields.forEach(({ name }) => {
    if (!values[name]) {
      errors[name] = `Required`;
    }
  });

  if (values.recipients) {
    errors.recipients = emalisValidation(values.recipients);
  }
  return errors;
}

export default reduxForm({
  validate: validate,
  form: "surveyForm",
  destroyOnUnmount: false,
})(SurveyForm);
