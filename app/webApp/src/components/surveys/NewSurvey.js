import React, { Component } from "react";
import SurveyFom from "./SurveyForm";
import SurveyReview from "./SurveyReview";
import { reduxForm } from "redux-form";

class NewSurvey extends Component {
  state = { showFormReview: false };

  renderContent() {
    return this.state.showFormReview ? (
      <SurveyReview onBack={() => this.setState({ showFormReview: false })} />
    ) : (
      <SurveyFom
        onSurveySubmit={() => this.setState({ showFormReview: true })}
      />
    );
  }

  render() {
    return <div>{this.renderContent()}</div>;
  }
}

export default reduxForm({
  form: "surveyForm",
})(NewSurvey);
