const regex =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default (emailString) => {
  const invalidEmalis = emailString
    .split(",")
    .map((email) => email.trim())
    .filter((email) => regex.test(email) === false);

  return invalidEmalis.length
    ? `These emails are invalid ${invalidEmalis}`
    : null;
};
