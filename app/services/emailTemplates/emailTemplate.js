module.exports = (survey) => {
  return `
  <html>
  <head>
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
    />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </head>
  <body>
    <div style="text-align: center">
      <h3>I'd like your input</h3>
      <p>Please answer the following question:</p>
      <p>${survey.body}</p>

      <div class="card-action">
        <a href="http://localhost:3000/api/surveys/${survey.id}/yes" class="waves-effect waves-light btn"
          >Yes</a
        >
        <a href="http://localhost:3000/api/surveys/${survey.id}/no" class="waves-effect waves-light btn"
          >No</a
        >
      </div>
    </div>
    <div style="margin-top: 20px;">
        <footer class="page-footer">
            <div class="footer-copyright">
              <div class="container">
              © 2014 Copyright Emaiyl.com
              <a class="grey-text text-lighten-4 right" href="#!">Emaly</a>
              </div>
            </div>
          </footer>
    </div>
  </body>
</html>
  `;
};
