const sendgrid = require("sendgrid");
const helper = sendgrid.mail;
const configs = require("../config/keys");

class Mailer extends helper.Mail {
  constructor({ subject, recipients }, template) {
    super();

    this.sendGridApi = sendgrid(configs.sendGridsApiKey);
    this.from_email = new helper.Email("khumalop315@gmail.com");
    this.subject = subject;
    this.body = new helper.Content("text/html", template);
    this.recipients = this.formatAddresses(recipients);
    
    this.addContent(this.body);
    this.addClickTracking();
    this.addRecipients();
  }

  formatAddresses(recipients) {
    return recipients.map(({ email }) => {
      return new helper.Email(email);
    });
  }

  addClickTracking() {
    const trackingSettings = new helper.TrackingSettings();
    const clickTracking = new helper.ClickTracking(true, true);

    trackingSettings.setClickTracking(clickTracking);
    this.addTrackingSettings(trackingSettings);
  }

  addRecipients() {
    const personalize = new helper.Personalization();

    this.recipients.forEach((recipient) => {
      personalize.addTo(recipient);
    });
    this.addPersonalization(personalize);
  }

  async send() {
    const request = this.sendGridApi.emptyRequest({
      method: "POST",
      path: "/v3/mail/send",
      body: this.toJSON(),
    });

    const response = await this.sendGridApi.API(request);

    return response;
  }
}

module.exports = Mailer;
